﻿using UnityEngine;

public sealed class AsteroidController : ProjectileController
{
    protected override void CalculatePosition()
    {
        Vector3 newPosition = transform.position + Vector3.down * Time.deltaTime * projectileModel.Speed;
        projectileView.UpdatePosition(newPosition);
    }
}
