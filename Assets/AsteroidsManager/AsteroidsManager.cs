﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public sealed class AsteroidsManager : ProjectileManager
{
    [SerializeField] private float spawnTimer = default; //in seconds
    [SerializeField] private Text asteroidsCountText;

    private int asteroidsToSpawn;
    private int asteroidsRemain;
    private LevelManagerController levelManager;

    private int levelBorder;

    protected override void Start()
    {
        base.Start();

        levelManager = DepContainer.LevelManagerController;
        levelManager.OnLevelRestart += OnLevelRestart;

        asteroidsToSpawn = levelManager.GetAsteroidsToSpawnCount();
        levelBorder = levelManager.GetLevelWidth() / 2;
        SetAsteroidsCount(asteroidsToSpawn);
        StartCoroutine(Spawn());
    }

    private IEnumerator Spawn()
    {
        while(asteroidsToSpawn > 0)
        {
            if (projectilesPool.Count > 0)
            {
                projectilesPool[0].Spawn(GetSpawnPosition());
                activeProjectiles.Add(projectilesPool[0]);
                projectilesPool.RemoveAt(0);
                asteroidsToSpawn--;
            }
            yield return new WaitForSeconds(spawnTimer);
        }

        yield return new WaitUntil(() => activeProjectiles.Count == 0);
        levelManager.OnLevelFinished(true);
    }

    public void OnLevelRestart()
    {
        StopAllCoroutines();

        for (int i = 0; i < activeProjectiles.Count; i++)
        {
            activeProjectiles[i].ToggleActive(false);
            projectilesPool.Add(activeProjectiles[i]);
        }
        activeProjectiles.Clear();
        asteroidsToSpawn = levelManager.GetAsteroidsToSpawnCount();
        SetAsteroidsCount(asteroidsToSpawn);
        StartCoroutine(Spawn());
    }

    private Vector3 GetSpawnPosition() => new Vector3(Random.Range(-levelBorder, levelBorder), 30, 0);

    public override void OnProjectileDestroyed(ProjectileController projectile)
    {
        base.OnProjectileDestroyed(projectile);
        SetAsteroidsCount(--asteroidsRemain);
    }

    private void SetAsteroidsCount(int count)
    {
        asteroidsRemain = count;
        asteroidsCountText.text = count.ToString();
    }
}
