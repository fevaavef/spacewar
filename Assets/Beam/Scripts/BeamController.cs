﻿using UnityEngine;

public sealed class BeamController : ProjectileController
{
    protected override void CalculatePosition()
    {
        Vector3 newPosition = transform.position + Vector3.up * Time.deltaTime * projectileModel.Speed;
        projectileView.UpdatePosition(newPosition);
    }
}
