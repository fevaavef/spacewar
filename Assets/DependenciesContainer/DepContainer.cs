﻿using UnityEngine;

public class DepContainer : MonoBehaviour //Simple dependencies container, added to scripts execution order
{
    public static LevelManagerController LevelManagerController { get; private set; }
    public static BeamsManager BeamsManager { get; private set; }
    public static MainMenuManager MainMenuManager { get; private set; }
    private void Awake()
    {
        LevelManagerController = FindObjectOfType<LevelManagerController>();
        BeamsManager = FindObjectOfType<BeamsManager>();
        MainMenuManager = FindObjectOfType<MainMenuManager>();
    }
}
