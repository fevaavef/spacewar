﻿using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManagerController : MonoBehaviour
{
    [SerializeField] private GameObject shipPrefab = default;
    [SerializeField] private string mapSceneName;
    [SerializeField] private BoxCollider2D asteroidsDisappearTrigger;
    [SerializeField] private BoxCollider2D beamsDisappearTrigger;

    public delegate void LevelState();
    public LevelState OnLevelRestart;

    private LevelManagerView levelManagerView;
    private bool isInMenu = false;

    private void Start()
    {
        levelManagerView = GetComponent<LevelManagerView>();
        levelManagerView.ReplayButton
            .OnClickAsObservable()
            .Subscribe(_ => RestartLevel())
            .AddTo(this);

        levelManagerView.ToMenuButton
            .OnClickAsObservable()
            .Subscribe(_ => ToMainMenu())
            .AddTo(this);

        InitializeShip();

        asteroidsDisappearTrigger.size = beamsDisappearTrigger.size =
            new Vector2(LevelsDataStorage.GetCurrentLevelData().levelWidth, .1f);
        ToggleCursor(false);
    }

    private void RestartLevel()
    {
        OnLevelRestart();
        levelManagerView.ToggleMenuPanel(false);
        ToggleCursor(false);
        isInMenu = false;
    }

    private void ToMainMenu() => SceneManager.LoadScene(mapSceneName);

    public void OnLevelFinished(bool isSuccessful)
    {
        if (!isInMenu)
        {
            ToggleCursor(true);
            levelManagerView.ToggleMenuPanel(true, isSuccessful);
            isInMenu = true;
            if(isSuccessful) LevelsDataStorage.UpdateCurrentLevelStatus(LevelStatus.Completed);
        }
    }

    private void InitializeShip()
    {
        var spawnedShip = Instantiate(shipPrefab, shipPrefab.transform.position, Quaternion.identity);
        var shipView = spawnedShip.GetComponent<ShipView>();

        shipView.Initialize(LevelsDataStorage.GetCurrentLevelData().levelWidth);
    }
    public int GetAsteroidsToSpawnCount() => LevelsDataStorage.GetCurrentLevelData().asteroidsCount;
    public int GetLevelWidth() => LevelsDataStorage.GetCurrentLevelData().levelWidth;

    private void ToggleCursor(bool visible)
    {
        Cursor.visible = visible;
        Cursor.lockState = visible ? CursorLockMode.None : CursorLockMode.Locked;
    }
}
