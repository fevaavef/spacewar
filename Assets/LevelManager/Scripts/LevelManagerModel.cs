﻿using UnityEngine;

public class LevelManagerModel : ScriptableObject
{
    [SerializeField] private float levelWidth;
    [SerializeField] private float asteroidsAmount;
}
