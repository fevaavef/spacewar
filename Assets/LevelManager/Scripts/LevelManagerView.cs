﻿using UnityEngine;
using UnityEngine.UI;

public class LevelManagerView : MonoBehaviour
{
    [SerializeField] private Button replayButton;
    [SerializeField] private Button toMenuButton;
    [SerializeField] private GameObject menuPanel;
    [SerializeField] private Text levelStatusText;

    public Button ReplayButton { get => replayButton; }
    public Button ToMenuButton { get => toMenuButton; }
    public void ToggleMenuPanel(bool active, bool success = false)
    {
        menuPanel.SetActive(active);
        levelStatusText.text = success ? "Level completed" : "Game over";
    }
}
