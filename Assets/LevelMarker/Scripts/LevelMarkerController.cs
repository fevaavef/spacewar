﻿using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelMarkerController : MonoBehaviour
{
    [SerializeField] private LevelMarkerModel levelMarkerModel;
    [SerializeField] private LevelMarkerView levelMarkerView;

    private MainMenuManager mainMenuController;

    private void Start()
    {
        mainMenuController = DepContainer.MainMenuManager;

        levelMarkerView.StartLevelButton
            .OnClickAsObservable()
            .Subscribe(_ => TryToStartLevel())
            .AddTo(this);
    }

    private void TryToStartLevel()
    {
        if (levelMarkerModel.Status != LevelStatus.Closed)
        {
            levelMarkerModel.OnLevelEntered();
            mainMenuController.LoadLevel(this);
        }
    }

    public LevelStatus GetStatus() => levelMarkerModel.Status;
    public int GetAsteroidsCount() => levelMarkerModel.AsteroidsCount;
    public int GetLevelWidth() => levelMarkerModel.LevelWidth;
    public void SetLoadedData(LevelStatus status, int asteroidsCount, int levelWidth)
    {
        levelMarkerModel.SetLoadedData(status, asteroidsCount, levelWidth);
        levelMarkerView.UpdateStatusImage(status);
    }
    public void UpdateStatus(LevelStatus status)
    {
        levelMarkerModel.Status = status;
        levelMarkerView.UpdateStatusImage(status);
    }

    public void ShowStatus() => levelMarkerView.UpdateStatusImage(levelMarkerModel.Status);
}
