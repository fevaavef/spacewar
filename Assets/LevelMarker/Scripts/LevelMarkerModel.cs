﻿using UnityEngine;

public class LevelMarkerModel : MonoBehaviour
{
    [SerializeField] private LevelStatus levelStatus;
    [SerializeField] private MinMaxPair asteroidsCountRange;
    [SerializeField] private MinMaxPair levelWidthRange;
    public LevelStatus Status
    {
        get
        {
            return levelStatus;
        }
        set
        {
            levelStatus = value;
        }
    }

    public int LevelWidth { get; private set; }
    public int AsteroidsCount { get; private set; }

    public void OnLevelEntered()
    {
        if(AsteroidsCount==0)
        {
            LevelWidth = Random.Range(levelWidthRange.min, levelWidthRange.max);
            AsteroidsCount = Random.Range(asteroidsCountRange.min, asteroidsCountRange.max);
        }
    }

    public void SetLoadedData(LevelStatus status, int asteroidsCount, int levelWidth)
    {
        Status = status;
        LevelWidth = levelWidth;
        AsteroidsCount = asteroidsCount;
    }

    [System.Serializable]
    private struct MinMaxPair
    {
        public int min;
        public int max;
    }
}
