﻿using UnityEngine.UI;
using UnityEngine;

public class LevelMarkerView : MonoBehaviour
{
    [SerializeField] private Image levelStatusImage;
    [SerializeField] private Button startLevelButton;

    public Button StartLevelButton { get=> startLevelButton; }

    public void UpdateStatusImage(LevelStatus status) => levelStatusImage.sprite = LevelStatusData.GetStatusSprite(status);
}
