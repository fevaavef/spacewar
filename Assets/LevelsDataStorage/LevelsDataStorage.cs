﻿public class LevelsDataStorage
{
    public static int CurrentLevelIndex { get; set; }

    private static StoragedLevelData[] storagedData;
    public static void InitializeStorage(LevelMarkerController[] levels)
    {
        storagedData = new StoragedLevelData[levels.Length];

        for (int i = 0; i < levels.Length; i++)
        {
            storagedData[i] = 
                new StoragedLevelData(i, levels[i].GetStatus(), levels[i].GetAsteroidsCount(), levels[i].GetLevelWidth());
        }
    }

    public static StoragedLevelData[] GetAllLevelsData() => storagedData;
    public static StoragedLevelData GetCurrentLevelData() => storagedData[CurrentLevelIndex];
    public static void UpdateCurrentLevelStatus(LevelStatus status) => storagedData[CurrentLevelIndex].status = status;
}

public struct StoragedLevelData
{
    public int id;
    public LevelStatus status;
    public int asteroidsCount;
    public int levelWidth;

    public StoragedLevelData(int id, LevelStatus status, int asteroidsCount, int levelWidth)
    {
        this.id = id;
        this.status = status;
        this.asteroidsCount = asteroidsCount;
        this.levelWidth = levelWidth;
    }
}
