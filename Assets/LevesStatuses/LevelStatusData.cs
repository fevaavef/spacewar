﻿using System.Collections.Generic;
using UnityEngine;

public class LevelStatusData : MonoBehaviour
{
    [SerializeField] private Sprite statusOpen;
    [SerializeField] private Sprite statusCompleted;
    [SerializeField] private Sprite statusClosed;

    private static Dictionary<LevelStatus, Sprite> statusDictionary;

    private void Awake()
    {
        statusDictionary = new Dictionary<LevelStatus, Sprite>()
        {
            { LevelStatus.Open, statusOpen },
            { LevelStatus.Completed, statusCompleted},
            { LevelStatus.Closed, statusClosed}
        };
    }
    public static Sprite GetStatusSprite(LevelStatus status) => statusDictionary[status];
}

public enum LevelStatus
{
    Open,
    Completed,
    Closed
}
