﻿using System;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class MainMenuManager : MonoBehaviour
{
    [SerializeField] private LevelMarkerController[] allLevelsByOrder;
    [SerializeField] private Button quitButton;
    [SerializeField] private string levelSceneName; 

    private static bool dataWasLoadedFromFile = false;
    private void Start()
    {
        quitButton.OnClickAsObservable().Subscribe(_ => OnQuit()).AddTo(this);
        LoadData();
        UpdateStatuses();
    }

    private void LoadData()
    {
        if (dataWasLoadedFromFile)
        {
            StoragedLevelData[] levelsData = LevelsDataStorage.GetAllLevelsData();
            for (int i = 0; i < levelsData.Length; i++)
            {
                allLevelsByOrder[i].SetLoadedData(levelsData[i].status, levelsData[i].asteroidsCount, levelsData[i].levelWidth);
            }
        }
        else
        {
            SavesManager.LoadProgress(allLevelsByOrder);
            dataWasLoadedFromFile = true;
        }
    }

    public void LoadLevel(LevelMarkerController level)
    {
        LevelsDataStorage.InitializeStorage(allLevelsByOrder);
        LevelsDataStorage.CurrentLevelIndex = Array.IndexOf(allLevelsByOrder, level);
        SceneManager.LoadScene(levelSceneName);
    }

    private void UpdateStatuses()
    {
        int indexOfLast = -1;
        for (int i = 0; i < allLevelsByOrder.Length; i++)
        {
            if (allLevelsByOrder[i].GetStatus() == LevelStatus.Completed)
                indexOfLast = i;
            print(allLevelsByOrder[i].GetStatus());
            allLevelsByOrder[i].ShowStatus();
        }

        if (indexOfLast == -1) allLevelsByOrder[0].UpdateStatus(LevelStatus.Open);
        else if (indexOfLast != allLevelsByOrder.Length - 1)allLevelsByOrder[indexOfLast + 1].UpdateStatus(LevelStatus.Open);
    }

    private void OnQuit()
    {
        SavesManager.SaveProgress(allLevelsByOrder);
        Application.Quit();
    }
}
