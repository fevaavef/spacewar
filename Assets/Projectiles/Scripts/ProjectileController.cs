﻿using UnityEngine;

public abstract class ProjectileController : MonoBehaviour
{
    [SerializeField] protected ProjectileModel projectileModel;

    protected ProjectileView projectileView;

    private ProjectileManager projectileManager;

    public void Initialize(ProjectileManager projectileManager) => this.projectileManager = projectileManager;
    private void Start() => projectileView = GetComponent<ProjectileView>();
    private void Update() => CalculatePosition();
    protected abstract void CalculatePosition();
    public void ToggleActive(bool activate) => gameObject.SetActive(activate);
    private void OnTriggerEnter2D(Collider2D collision)
    {
        projectileManager.OnProjectileDestroyed(this);
        ToggleActive(false);
    }
    public void Spawn(Vector3 position)
    {
        ToggleActive(true);
        transform.position = position;
    }
}
