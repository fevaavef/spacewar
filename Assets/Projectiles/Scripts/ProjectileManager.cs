﻿using System.Collections.Generic;
using UnityEngine;

public abstract class ProjectileManager : MonoBehaviour
{
    [SerializeField] protected GameObject projectilePrefab;
    [SerializeField] private int maxProjectilesOnScene;

    protected List<ProjectileController> projectilesPool = new List<ProjectileController>();
    protected List<ProjectileController> activeProjectiles = new List<ProjectileController>();

    protected virtual void Start()
    {
        for (int i = 0; i < maxProjectilesOnScene; i++)
        {
            ProjectileController spawnedProjectile = Instantiate(projectilePrefab, Vector3.zero, Quaternion.identity)
                .GetComponent<ProjectileController>();
            projectilesPool.Add(spawnedProjectile);
            spawnedProjectile.ToggleActive(false);
            spawnedProjectile.Initialize(this);
        }
    }

    public void SpawnProjectile(Vector3 position)
    {
        if (projectilesPool.Count > 0)
        {
            projectilesPool[0].Spawn(position);
            activeProjectiles.Add(projectilesPool[0]);
            projectilesPool.RemoveAt(0);
        }
    }

    public virtual void OnProjectileDestroyed(ProjectileController projectile)
    {
        activeProjectiles.Remove(projectile);
        projectilesPool.Add(projectile);
    }
}
