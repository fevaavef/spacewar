﻿using UnityEngine;

public abstract class ProjectileModel : ScriptableObject
{
    [SerializeField] private float speed;
    public float Speed { get => speed; }
}
