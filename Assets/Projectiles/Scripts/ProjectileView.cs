﻿using UnityEngine;

public abstract class ProjectileView : MonoBehaviour
{
    public void UpdatePosition(Vector3 newPosition)
    {
        transform.position = newPosition;
    }
}
