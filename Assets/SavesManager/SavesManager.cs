﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class SavesManager : MonoBehaviour
{
    public static void SaveProgress(LevelMarkerController[] levelsData)
    {
        LevelData[] dataToSave = new LevelData[levelsData.Length];

        for (int i = 0; i < dataToSave.Length; i++)
            dataToSave[i] = new LevelData(levelsData[i].GetStatus(), levelsData[i].GetAsteroidsCount(), levelsData[i].GetLevelWidth());

        string folder = "SavedData";
        if (!Directory.Exists(folder)) Directory.CreateDirectory(folder);
        string dataPath = Path.Combine(folder, "GameState.dat");
        BinaryFormatter binaryFormatter = new BinaryFormatter();

        using (FileStream fileStream = File.Open(dataPath, FileMode.OpenOrCreate))
        {
            binaryFormatter.Serialize(fileStream, dataToSave);
        }
    }

    public static void LoadProgress(LevelMarkerController[] levelsToSetData)
    {
        LevelData[] loadedData;

        BinaryFormatter binaryFormatter = new BinaryFormatter();
        string folder = "SavedData";
        string dataPath = Path.Combine(folder, "GameState.dat");

        if (!File.Exists(dataPath)) return;

        using (FileStream fileStream = File.Open(dataPath, FileMode.Open))
        {
            loadedData = (LevelData[])binaryFormatter.Deserialize(fileStream);
        }

        if (levelsToSetData.Length != loadedData.Length) Debug.LogError("Saved data was corrupted");
        else
        {
            for (int i=0; i<levelsToSetData.Length; i++)
            {
                levelsToSetData[i].SetLoadedData(loadedData[i].status, loadedData[i].asteroidsCount, loadedData[i].levelWidth);
            }
        }
    }

    [System.Serializable]
    private struct LevelData
    {
        public LevelStatus status;
        public int asteroidsCount;
        public int levelWidth;

        public LevelData(LevelStatus status, int asteroidsCount, int levelWidth)
        {
            this.status = status;
            this.asteroidsCount = asteroidsCount;
            this.levelWidth = levelWidth;
        }
    }
}


