﻿using System.Collections;
using UnityEngine;

public class ShipController : MonoBehaviour
{
    [SerializeField] private ShipModel shipModel;
    [SerializeField] private Transform beamSpawnPoint;

    private ShipView shipView;
    private BeamsManager beamsManager;
    private bool readyToShoot = true;
    private LevelManagerController levelManagerController;
    private bool isAlive = true;

    private void Start()
    {
        levelManagerController = DepContainer.LevelManagerController;
        levelManagerController.OnLevelRestart += OnLevelRestart;

        shipView = GetComponent<ShipView>();
        beamsManager = DepContainer.BeamsManager;
        shipView.UpdateHPText(shipModel.HP);
    }

    private void OnHit()
    {
        shipModel.OnHit();
        shipView.UpdateHPText(shipModel.HP);
        if (shipModel.HP == 0) SetDestroyed();
    }

    private void SetDestroyed()
    {
        isAlive = false;
        shipView.ToggleVisible(false);
        levelManagerController.OnLevelFinished(false);
    }

    private void Update()
    {
        if (isAlive)
        {
            HandleControlsInput();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        OnHit();
    }

    private void HandleControlsInput()
    {
        if (Input.GetButtonDown("Shoot")) Shoot();

        Vector3 newPosition = transform.position +
            Input.GetAxis("Horizontal") * shipModel.Speed * Vector3.right * Time.deltaTime;

        shipView.UpdatePosition(newPosition);
    }

    private void Shoot()
    {
        if (readyToShoot)
        {
            readyToShoot = false;
            beamsManager.SpawnProjectile(beamSpawnPoint.transform.position);
            StartCoroutine(CoolDownShooting());
        }
    }

    private IEnumerator CoolDownShooting()
    {
        yield return new WaitForSeconds(shipModel.CoolDownTime);
        readyToShoot = true;
    }

    private void OnLevelRestart()
    {
        shipModel.RestoreHP();
        shipView.UpdateHPText(shipModel.HP);
        shipView.ToggleVisible(true);
        isAlive = true;
    }
}
