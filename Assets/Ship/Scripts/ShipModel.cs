﻿using UnityEngine;

[CreateAssetMenu]
public class ShipModel : ScriptableObject
{
    [SerializeField] private int maxHitPoints;
    [SerializeField] [Range(1f, 10f)] private float speed;
    [SerializeField] private float shootCoolDownTime; //in seconds

    private void OnEnable()
    {
        HP = maxHitPoints;
    }

    public void OnHit() => HP--;
    public void RestoreHP() => HP = maxHitPoints;
    public int HP { get; private set; }
    public float Speed { get => speed; }
    public float CoolDownTime { get => shootCoolDownTime; }
}
