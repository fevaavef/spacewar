﻿using UnityEngine;
using UnityEngine.UI;

public class ShipView : MonoBehaviour
{
    [SerializeField] private Text hpText;
    [SerializeField] private Renderer selfImage;
    [SerializeField] private Collider2D selfCollider;

    private float maxDistanceFromCenter;

    public void Initialize(float levelWidth) => maxDistanceFromCenter = levelWidth / 2f;
    public void UpdateHPText(float newHP) => hpText.text = newHP.ToString();
    public void UpdatePosition(Vector3 newPosition)
    {
        newPosition.x = Mathf.Abs(newPosition.x) > maxDistanceFromCenter ? transform.position.x : newPosition.x;
        transform.position = newPosition;
    }

    public void ToggleVisible(bool active) => selfCollider.enabled = selfImage.enabled = active;
}
